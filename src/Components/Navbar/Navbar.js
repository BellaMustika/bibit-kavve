import React from "react";
import {
  Navbar, 
  Nav,
  Container,
  Image,
  Row, 
  Col,
  div, 
  Form, 
  Button,
  Card
} from 'react-bootstrap';

function Header(){
  return(
    <div className="App">
      <Navbar bg="white" expand="lg" style={{ height:"70px", position:"top"  }}>
        <Container>
          <Navbar.Brand href="http://localhost:3001/">
            <img
              src="https://bibit.id/img/logoBibitFix.svg"
              textAlign="center"
              width="100"
              margin="14"
              className="d-inline-block align-top"/>
          </Navbar.Brand>

          <Nav>
            <Nav.Link href="#Reksadana" style={{ color: '#00ab6b', margin: "18px" }}>Reksadana</Nav.Link>
            <Nav.Link href="#Blog" style={{ color: '#00ab6b', margin: "18px" }}>Blog</Nav.Link>
            <Nav.Link href="#FAQ" style={{ color: '#00ab6b', margin: "18px" }}>FAQ</Nav.Link>
            <Nav.Link href="#Karir" style={{ color: '#00ab6b', margin: "18px" }}>Karir</Nav.Link>
            <Button variant="outline-success" style={{ margin: "18px" }}>Login</Button>
          </Nav>
        </Container>
      </Navbar> 
    </div>
  );
}


export default Header;