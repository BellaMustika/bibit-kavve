import React from "react";
import {Navbar, Nav, Image, Row, Col, div, Form, Button} from 'react-bootstrap';


function Banner(){
  return(
    <Row>
      <Col style= {{  padding: "110px", paddingTop: "10x  0px" }}>
        <h1 style={{ color: "#00ab6b", fontWeight:'bold'}}>INVESTASI REKSA DANA <br/>SECARA OTOMATIS</h1>
          <br/>
        <p style={{ color: "#797A81", fontfamily: "arial", fontSize:"25px" }}>Bibit menempatkan uang kamu ke portfolio <br/>reksa dana secara cerdas dan tanpa ribet.</p>
          <br/>

        <Navbar.Brand href="https://play.google.com/store/apps/details?id=com.bibit.bibitid">
          <img
            src="https://bibit.id/img/playStore.png"
            width="150"
            height="45"
            className="d-inline-block align-top"/>
        </Navbar.Brand>

        <Navbar.Brand href="https://apps.apple.com/id/app/bibit-investasi-reksadana/id1445856964">
          <img
            src="https://bibit.id/img/appStore.png"
            width="150"
            height="45"
            className="d-inline-block align-top"/> 
        </Navbar.Brand>
          <br/><br/>
        <p style={{ color: "gray" }}>Terdaftar dan Diawasi oleh</p>
          <br/>

        <Navbar.Brand href="https://reksadana.ojk.go.id/Public/APERDPublic.aspx?id=BTB69">
          <img
            src="https://bibit.id/img/ojk.png"
            width="120"
            height="50"
            className="d-inline-block align-top"/>
        </Navbar.Brand>
      </Col>

      <Col>
        <div className="shape-banner">
          <Image src="https://bibit.id/img/mockup.png" style={{ marginLeft:"73px", width:"270px", paddingBottom: "100px", paddingTop:"20px" }} bsPrefix/>
        </div>
      </Col>
    </Row>
  );
}

export default  Banner;