import React from "react";
import {Navbar, Nav, Image, Row, Col, div, Form, Button} from 'react-bootstrap';

function Main(){
  const Slide = (props) => {
    return(
      <div className= "Container">
        <div className="row-col-flex margin-auto">
          <div style={{ width: "50 0px", paddingTop: '150px' }}>
            <div class="content-wrap">
              <div class="content-title">
                <Image class="content-icon" src={props.Image} alt="icon processor" width="50px" padding="100px"/>
                <h3 style={{ color: "#2478B6" }}>{props.Title}</h3>
              </div>

              <div class="content-desc">
                <p>{props.Subtitle}</p>
              </div>
            </div>    
          </div>
        </div>
      </div>
    );
  }

  return(
    <Row>
      <Row>
        <Col>
          <Image src="https://bibit.id/img/illustration2.svg" fluid width="500"/>
        </Col>

        <Col>
          <Slide 
            Image="https://bibit.id/img/processorIcon.png"
            Title="TEKNOLOGI YANG DIDUKUNG OLEH RISET PEMENANG NOBEL PRIZE"
            Subtitle="Teknologi kami menggunakan pendekatan Teori Modern Portfolio yang diperkenalkan oleh ekonom Harry Markowitz yang sudah terbukti dapat menjaga resiko dan memaksimalkan keuntungan kamu lewat diversifikasi."/>
        </Col>
      </Row>

      <Row style={{ marginRight: "70px", marginLeft: "30px" }}>
        <Col style={{ textAlign: "Right" }}>
          <Slide 
            Image="https://bibit.id/img/formulaIcon.png"
            Title="INVESTASI YANG DIRANCANG KHUSUS UNTUK KAMU"
            Subtitle="Setiap orang mempunyai profil yang berbeda. Kita bantu menempatkan uang kamu ke portfolio reksadana yang khusus dibuat untuk kamu sesuai dengan umur, penghasilan dan toleransi kamu terhadap resiko. Secara otomatis"/>
        </Col>

        <Col>
          <Image src="https://bibit.id/img/illustration3.svg" fluid width="500"/>
        </Col>
      </Row>

      <Row>
       <Col>
          <Image src="https://bibit.id/img/illustration4.svg" fluid width="530"/>
        </Col>

        <Col>
          <Slide 
            Image="https://bibit.id/img/machineIcon.png"
            Title="REBALANCING OTOMATIS"
            Subtitle="Teknologi kami akan bekerja dan memantau portfolio kamu secara otomatis untuk mempertahankan alokasi optimal kamu."/>
        </Col>
      </Row>

      <Row style={{ marginLeft: "100px", paddingBottom: "200px" }}>
        <Col style={{ textAlign: "Right" }}>
          <Slide 
            Image="https://bibit.id/img/fastIcon.png"
            Title="BUKA AKUN SECARA INSTAN"
            Subtitle="Pembukaan rekening secara digital dan dapat diselesaikan dalam hitungan menit. Tanpa formulir, tanpa financial planner, tanpa spreadsheet. Tanpa ribet."/>
        </Col>

        <Col style={{ width: "60px", paddingTop: "70px" }}>
          <Image src="https://bibit.id/img/illustration5.svg" fluid width="300"/>
        </Col>
      </Row>
    </Row>
  );

}

export default Main;