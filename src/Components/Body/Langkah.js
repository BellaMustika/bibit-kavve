import React from "react";
import {Navbar, Nav, Image, Row, Col, div, Form, Button} from 'react-bootstrap';

function Langkah(){
  const  Rutif = (props) => {
    return(
      <div className="Container">
        <div class="box-number">
          <Image class="number-icon" src={props.Image} alt="icon order one" width= "40px"/>
        
          <div class="shadow-box">
            <h6>{props.Title}</h6>
            <p>{props.Subtitle}</p>
          </div>
        </div>
      </div>
    );
  }

  return(
    <Row>
      <Col>
        <Rutif
        Image="https://bibit.id/img/orderOne.png"
        Title="Kita Pelajari Profil Kamu"
        Subtitle="Kita ingin mengetahui tujuan investasi kamu, umur kamu dan pandangan kamu terhadap resiko"/>
      </Col>

      <Col>
        <Rutif
        Image="https://bibit.id/img/orderTwo.png"
        Title="Kita Bangun Portfolio Investasi Kamu"
        Subtitle="Kita berikan saran investasi yang optimal berdasarkan toleransi kamu terhadap resiko"/>
      </Col>

      <Col>
        <Rutif
        Image="https://bibit.id/img/orderThree.png"
        Title="Uang Kamu Mulai Bekerja Lebih Keras Untuk Kamu"
        Subtitle="Set up sekali dan mulai lihat bibit masa depan kamu bertumbuh tanpa ribet"/>
      </Col>
    </Row>
  );
}

export default Langkah;