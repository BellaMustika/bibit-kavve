import React from "react";
import {Navbar, Nav, Image, Row, Col, div, Form, Button, Container, Card} from 'react-bootstrap';

function Feature(){
  const  Sorot = (props) => {
    return(
		<div style={{ marginLeft: "70px", marginRight: "40px" }}>
			<div class="card" style={{ width: "500px", height: "140px", borderRadius: "15px", boxShadow: "0px 1px #F0F0F0", marginLeft: "10px", marginTop: "20px"}}>
        		<Card.Img variant="top" src={props.Image} style={{ maxWidth: "60px", margin: "20px" }} />
    		</div>
      
		    <div class="col-md-8">
		  		<Card.Body>
	        		<Card.Title><h6>{props.Title}</h6></Card.Title>
	        		<Card.Text>
	          			<p style={{ fontSize: "13px" }}>{props.Subtitle}</p>
		        	</Card.Text>
		      	</Card.Body>
		   	</div>
  		</div>
      
    );
  }

  return(
    <Row>
    	<Row>
      		<Col>
        		<Sorot
          			Image="https://bibit.id/img/rpNol.svg"
          			Title="Gratis Biaya Komisi"
          			Subtitle="Biaya Rp0. Semua pembelian reksadana di Bibit gratis biaya komisi."/>
      		</Col>
    	</Row>

    	<Row>
      		<Col>
        		<Sorot
          			Image="https://bibit.id/img/minimInvestasi.svg"
          			Title="Dana Investasi yang Minim"
          			Subtitle="Kamu bisa mulai dengan dana serendah Rp10.000 dulu sampai kamu nyaman berinvestasi. Yang penting mulai."/>
      		</Col>
    	</Row>

    	<Row>
      		<Col>
        		<Sorot
          			Image="https://bibit.id/img/cairkanInvestasi.svg"
          			Title="Cairkan Investasimu Kapan Saja"
        		  	Subtitle="Dana investasi dapat dicairkan kapanpun dengan cepat tanpa penalti."/>
      		</Col>
    	</Row>

    	<Row>
      		<Col>
        		<Sorot 
          			Image="https://bibit.id/img/taxFree.svg"
        	  		Title="Tidak Dipajak"
        		  	Subtitle=" Berbeda dengan investasi properti ataupun emas. Berbeda dengan investasi properti"/>
      		</Col>
    	</Row>
    </Row>
  ); 
}

export default Feature;