import React from "react";
import {Image} from 'react-bootstrap';

function Keuntungan(){
  return(
    <div style={{ textAlign: "center", padding: "100px" }}>
    	<Image src= "https://bibit.id/img/ideaIcon.png" fluid width= "45px"/>
    		<br/><br/>
    	<h6 style={{ color: '#4E83B8' }}> KEUNTUNGAN INVESTASI DENGAN BIBIT </h6>
    </div>
  );
}

export default Keuntungan;