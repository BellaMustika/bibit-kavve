import React from "react";
import {Image, Row,Col} from 'react-bootstrap';

function Keamanan(){
  const Liad = (props) => {
    return(
      	<div className= "Container">
      		<div className="row-col-flex margin-auto">
      			<div style={{ width: "50 0px", paddingTop: '150px' }}>
        			<div class="content-wrap">
            			<div class="content-title">
              				<Image class="content-icon" src={props.Image} alt="icon processor" width="50px" padding="100px"/>
              				<h3 style={{ color: "#2478B6" }}>{props.Title}</h3>
            			</div>

       	 				<div class="content-desc">
          					<p>{props.Subtitle}</p>
        				</div>
        			</div>    
      			</div>
      		</div>
      	</div>
    );
  }

  return(
    <Row>
        <Col>
          <Image src="https://bibit.id/img/illustration8.svg" fluid width="500" style={{ paddingTop:"100px" }}/>
        </Col>

        <Col>
          <Liad
            Image="https://bibit.id/img/shieldIcon.png"
            Title="KEAMANAN DANA DARI BANK"
            Subtitle="Bibit tidak menyimpan dana kamu. Dana yang kamu investasikan ditransfer langsung ke kustodian bank dan dikelola oleh perusahaan berlisensi OJK. Dana hanya dapat dicairkan ke rekening atas nama kamu."
          />
        </Col>
    </Row>
  );
}

export default Keamanan;