import React from "react";
import {
  Navbar, 
  Nav,
  Container,
  Image,
  Row, 
  Col,
  div, 
  Form, 
  Button,
  Card
} from 'react-bootstrap';

function Footer(){
  return(
    <div className= "App">
      <Container>
         <Row style={{ paddingBottom: "50px", paddingTop: "40px"}}>
             <Col><Image src="https://bibit.id/img/logoBibitFix.svg" width="70px" bsPrefix/> Copyright © 2020</Col>

            <div style={{cssFloat:"right" }}>    
              <Col>a<Image src="https://stockbit.com/assets/img/stockbit.svg" width="70px" bsPrefix/>Company</Col>
            </div>
         </Row>
        
         <Row>
              <p style={{ fontFamily: "arial", color: "gray"}}>PT Bibit Tumbuh Bersama (“Bibit”) berlaku sebagai Agen Penjual Efek Reksa Dana (APERD) yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan.</p>

              <p style={{ fontFamily: "arial", color: "gray"}}>Semua Investasi mengandung risiko dan kemungkinan kerugian nilai investasi. Kinerja pada masa lalu tidak mencerminkan 
              kinerja di masa depan. Kinerja historikal, expected return dan proyeksi probabilitas disediakan untuk tujuan informasi dan 
              illustrasi. Reksa dana merupakan produk pasar modal dan bukan produk APERD. APERD tidak bertanggung jawab atas risiko 
              pengelolaan portofolio yang dilakukan oleh Manajer Investasi.</p>
         </Row>
      </Container>
    </div>
  );
}


export default  Footer;