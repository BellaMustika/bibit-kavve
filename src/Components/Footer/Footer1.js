import React from "react";
import {Navbar, Nav, Image, Row, Col, div, Form, Button, Container} from 'react-bootstrap';

function Footer1(){
  return(
    <div className="bg">
      <Container>
        <Row>
           <Col> 
            	<p style={{fontSize:"20px", color:"#2478b4", marginLeft:"40px"}}>Masih Ragu dan Memiliki Pertanyaan?</p>
            	<h3 style={{fontSize:"20px", marginLeft:"40px"}}>Tenang, Kami selalu ada disini khusus buat kamu</h3>

            	<p class="text-muted" style={{fontSize:"16px",marginLeft:"40px"}}>Hubungi kami kapanpun:</p>			

            	<ul style={{listStyleType:"none"}}>
            		<li>
                		<Image src="https://bibit.id/img/icon-mail2.svg"/>
                		<a href="#" style={{color:"#00ab6b",fontSize:"18px", marginLeft:"9px"}}>hello@bibit.id</a>
                	</li>

               		<li>
                		<Image src="https://bibit.id/img/icon-wablue.svg"/>
                		<a href="#" style={{color:"#00ab6b",fontSize:"18px", marginLeft:"9px"}}>6221 50864230</a>
                		<h6 style={{fontSize:"14px",color:"#00ab6b",  marginLeft:"5px", marginLeft:"35px" }}>Via WhatsApp</h6>
               		</li>

               		<li>
               			<Image src="https://bibit.id/img/icon-tower.svg" width="25px"/>
                		<h7 style={{color:"#00ab6b",fontSize:"18px", marginLeft:"9px"}}>Menara Standard Chartered 35th floor</h7><br/>
                		<h7 style={{color:"#00ab6b",fontSize:"13px",letterSpacing:"1.2px", marginLeft:"30px"}}>Jl. Prof. Dr. Satrio No 164</h7><br/>
                		<h7 style={{color:"#00ab6b",fontSize:"13px",letterSpacing:"1.2px", marginLeft:"30px"}}>Jakarta 12930 </h7>
               		</li>
             	</ul>
           	</Col> 

           	<Col>
             	<ul style={{listStyleType:"none"}}>
              		<li>
                		<h3 style={{color:"#00ab6b",fontSize:"17px"}}>Apa itu redaksana?</h3>
                		<p style={{color:"gray"}}>Reksadana merupakan tempat pengelolaan dana atau modal bagi sekumpulan investor untuk berinvestasi dalam instrumen investasi yang tersedia di pasar.</p><br/>
              		</li>

              		<li><h4 style={{fontSize:"17px",color:"gray"}}>Bagaimana cara kerja reksadana?</h4></li><br/>
             	 	<li><h4 style={{fontSize:"17px",color:"gray"}}>Apa saja resiko yang harus dihadapi saat berinvestasi di reksadana?</h4></li><br/>
              		<li><a href="#" style={{color:"#00ab6b"}}>Cari tahu artikel lain -></a></li>
             	</ul>
           </Col>
        </Row> 
        	<br/>
        <Row>
          	<div style={{width:"500px"}}>
          		<Col>
            		<ul style={{listStyleType:"none"}}>
              			<li><p style={{fontWeight:"bold",fontSize:"15px",color:"#8998aa"}}>TERDAFTAR DAN DIAWASI OLEH</p></li>
              			<li><Image src="https://bibit.id/img/ojk.png" width="130px"/></li><br/>
              			<li><a href="#" style={{color:"#00ab6b",fontWeight:"bold"}}>KEP-14/PM.21/2017</a></li>
            		</ul>
          		</Col>
          	</div>
          
          	<Col>
            	<ul style={{listStyleType:"none"}}>
	              	<li><h3 style={{fontWeight:"bold",fontSize:"15px",color:"#8998aa"}}>IKUTI KAMI</h3></li>

	             	<li>
	                	<Image src="https://bibit.id/img/facebook.png" width="15px"/> 
	                	<a href="#" style={{color:"#8998aa",fontSize:"15px", marginLeft:"5px"}}>Facebook</a>
	              	</li>

	              	<li>
	                	<Image src="https://bibit.id/img/twitter.png" width="15px"/>  
	                	<a href="#" style={{color:"#8998aa",fontSize:"15px", marginLeft:"5px"}}>Twitter</a>
	              	</li>

	              	<li>
	                	<Image src="https://bibit.id/img/instagram.png" width="15px"/> 
	                	<a href="#" style={{color:"#8998aa",fontSize:"15px", marginLeft:"5px"}}>Instagram</a>
	              	</li>

	              	<li>
	                	<Image src="https://bibit.id/img/youTube.png" width="15px"/> 
	                	<a href="#" style={{color:"#8998aa",fontSize:"15px", marginLeft:"5px"}}>Youtube</a>
	              	</li>

	              	<li>
	                	<Image src="https://bibit.id/img/linkedIn.png" width="15px"/> 
	                	<a href="#" style={{color:"#8998aa",fontSize:"15px", marginLeft:"5px"}}>Linkedln</a>
	              	</li>

	              	<li>
	                	<Image src="https://bibit.id/img/telegram-icon.png" width="15px"/> 
	                	<a href="#" style={{color:"#8998aa",fontSize:"15px", marginLeft:"5px"}}>Telegram</a>
	              	</li>
            	</ul>
          	</Col>

          	<Col>
            	<ul style={{listStyleType:"none"}}>
              		<li><h3 style={{fontWeight:"bold",fontSize:"15px",color:"#8998aa"}}>PERUSAHAAN</h3></li>
	              	<li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Blog</a></li>
	              	<li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Karir</a></li>
	            </ul>
          	</Col>
          
          	<Col>
	            <ul style={{listStyleType:"none"}}>
	              <li><h3 style={{fontWeight:"bold",fontSize:"15px",color:"#8998aa"}}>DUKUNGAN</h3></li>
	              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>FAQ</a></li>
	              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Kebijakan dan Privasi</a></li>
	              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Syarat dan Ketentuan</a></li>
	            </ul>
          	</Col>
        </Row>       

      </Container>
    </div>
    );
}

export default  Footer1;