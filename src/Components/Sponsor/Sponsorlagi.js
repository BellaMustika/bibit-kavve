import React from "react";
import {Image} from 'react-bootstrap';

function Sponsorlagi(){
  return(
    <div style={{ textAlign: "center", paddingTop: "100px", paddingBottom: "100px" }}>
      <h7>Partner Kami</h7><br/><br/>
        <Image src= "https://bibit.id/img/manager-investasi/sinarmas.jpg" style={{ width: "130px", marginLeft: "50px" }}/>
        <Image src= "https://bibit.id/img/manager-investasi/schroders.png" style={{ width: "130px", marginLeft: "50px" }}/>
        <Image src= "https://bibit.id/img/manager-investasi/bni-am.jpg" style={{ width: "130px", marginLeft: "50px" }}/>
        <Image src= "https://bibit.id/img/manager-investasi/victoria.jpg" style={{ width: "130px", marginLeft: "50px" }}/>
        <Image src= "https://bibit.id/img/manager-investasi/mandiri-investasi.png" style={{ width: "100px", marginLeft: "50px" }}/>
  	</div>
  );
}

export default Sponsorlagi;