import React from "react";
import {Image, Row, Col, Card, Navbar} from 'react-bootstrap';

function Kartu(){
  return(
    <div style={{ paddingTop: "100px", marginLeft: "300px" }}>
      <Card body style={{ width: "700px", borderRadius: "15px", boxShadow: "0px 1px #F0F0F0" , textAlign: "center" }}>
        <Card.Title style={{ color: "green" }}>Mulai Tanam Bibit Investasimu</Card.Title><br/>
          <Navbar.Brand href="https://play.google.com/store/apps/details?id=com.bibit.bibitid">
            <img
              src="https://bibit.id/img/playStore.png"
              width="160"
              height="45"
              className="d-inline-block align-top"/>
          </Navbar.Brand>

          <Navbar.Brand href="https://apps.apple.com/id/app/bibit-investasi-reksadana/id1445856964">
            <img
              src="https://bibit.id/img/appStore.png"
              width="160"
              height="45"
              className="d-inline-block align-top"/> 
          </Navbar.Brand>
      </Card>
    </div>
  );
}

export default Kartu;