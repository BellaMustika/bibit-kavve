import React from "react";
import {Image} from 'react-bootstrap';


function Sponsor(){
  return(
    <div style={{ textAlign: "center", paddingTop: "100px" }}>
      <div>
        <h7>Telah diliputi oleh</h7><br/><br/>
          <Image src= "https://bibit.id/img/cnbc.png" style={{ width: "100px", marginLeft: "50px" }}/>
          <Image src= "https://bibit.id/img/dailysocial.png" style={{ width: "100px", marginLeft: "50px" }}/>
          <Image src= "https://bibit.id/img/deal-street.png" style={{ width: "100px", marginLeft: "50px" }}/>
      </div>
    </div>
  );
}

export default Sponsor;