import React from 'react';
import './App.css';
import Header from "./Components/Navbar/Navbar";
import Banner from "./Components/Body/Banner";
import Main from "./Components/Body/Main";
import Langkah from "./Components/Body/Langkah";
import Keuntungan from "./Components/Body/Keuntungan";
import Feature from "./Components/Body/Feature";
import Keamanan from "./Components/Body/Keamanan";
import Sponsor from "./Components/Sponsor/Sponsor";
import Kartu from "./Components/Sponsor/Kartu";
import Sponsorlagi from "./Components/Sponsor/Sponsorlagi";
import Footer1 from "./Components/Footer/Footer1";
import Footer from "./Components/Footer/Footer";
import {Navbar, Nav, Container, Image, Row, Col, div, Form, Button, Card} from 'react-bootstrap';

function App(){
  return(
    <div>
      <div className="sticky-top">
          <Header/>
      </div>
        <Banner/>
        <Main/>
        <Langkah/>
        <Keuntungan/>
        <Feature/>
        <Keamanan/>
        <Sponsor/>
        <Kartu/>
        <Sponsorlagi/>
        <Footer1/>
        <Footer/>
    </div>
  );
}

export default App;